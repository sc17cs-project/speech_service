const express = require('express');
const proxy = require('express-http-proxy');

const app = express();
const port = 1221;

app.use('/', proxy('http://speech.onlyke.com:1221'));

app.listen(port, () => console.log(`Demo Speech Service listening on port ${port}.`));
