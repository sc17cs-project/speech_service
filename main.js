const express = require('express');
const speech = require('@google-cloud/speech');
const textToSpeech = require('@google-cloud/text-to-speech');
const request = require('superagent');
const ttsClient = new textToSpeech.TextToSpeechClient();

const app = express();
const bodyParser = require("body-parser");
const port = 1221;

const sttConfig = {
    encoding: 'LINEAR16',
    sampleRateHertz: 16000,
    languageCode: 'en-US',
    model: 'phone_call',
    useEnhanced: true,
    speechContexts: [
        {
            phrases: [
                "elevator",
                "level",
                "floor",
                "button",
                "pressed"
            ]
        }
    ]
};
const sttClient = new speech.SpeechClient();

app.use(bodyParser.urlencoded({extended: false, limit: '5mb'}));
app.use(bodyParser.json());

app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Something broke!');
});


app.get('/ping', (req, res) => {
    res.status(204).send('');
});

// Access to the LUIS service
app.post('/intent', (req, res) => {
    request
        .get('https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/5243d353-80cc-48af-a5d4-f703911eadb5')
        .set('Ocp-Apim-Subscription-Key', '73abc6c7234d4c6fbc849a9956207417')
        .query({
            "q": req.body.message
        })
        .then(response => {
            if (response.ok) {
                const result = response.body;
                console.log(`LUIS: ${JSON.stringify(result)}`);
                res.json(result)
            } else {
                console.error(response.text);
                res.status(404).end('No result')
            }
        })
        .catch(err => {
            console.error(err);
            res.status(500).end('LUIS failed')
        });
});

// Access to Google STT
app.post('/stt', (req, res) => {
    const request = {
        config: sttConfig,
        audio: {
            content: req.body.buffer
        },
    };
    sttClient.recognize(request, function (error, response) {
        if (error) {
            console.error(error);
            res.status(500).end('STT failed');
        } else {
            const result = response.results
                .map(result => result.alternatives[0].transcript)
                .join('.').trim();
            if (result.length > 0) {
                res.json({
                    "result": result
                })
            } else {
                res.status(404).end('No result')
            }
        }
    });
});

// Access to the Google TTS
app.post('/tts', (req, res) => {
    const text = req.body.text;
    const request = {
        input: {text: text},
        // Select the language and SSML Voice Gender (optional)
        voice: {
            languageCode: 'en-US',
            name: "en-US-Wavenet-D",
            ssmlGender: 'NEUTRAL'
        },
        // Select the type of audio encoding
        audioConfig: {
            audioEncoding: 'MP3',
            speakingRate: 0.9,
            volumeGainDb: 5.0
        },
    };

    ttsClient.synthesizeSpeech(request).then(response => {
        res.writeHead(200, {
            'Content-Type': 'audio/mp3',
            'Content-disposition': `attachment;filename=output-${Math.round(new Date().getTime() / 1000).toString()}.mp3`
        });
        res.end(response[0].audioContent);
    }).catch(error => {
        console.error(error);
        res.status(500).end('TTS failed');
    });
});


app.listen(port, () => console.log(`Speech Service listening on port ${port}.`));