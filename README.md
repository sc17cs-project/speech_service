# Human-Robot Interaction through Speech

- Author: Chengke Sun
- Email: sc17cs@leeds.ac.uk

## Speech Service
Provide HTTP-based proxy for external speech services.

## License
This project is licensed under the [MIT License](LICENSE)


## How to use the demo service?

Open a new terminal and run the following command
```
npm run demo
```

The demo service is provided by Chengke Sun for testing purposes only. The demo service will no longer be guaranteed to be available after **December 2019**.

## How to build your own Speech Service

You can visit the following link to use your own Google TTS and Google STT service.

https://cloud.google.com/text-to-speech/docs/quickstart-client-libraries#client-libraries-install-nodejs

https://cloud.google.com/speech-to-text/docs/quickstart-client-libraries#client-libraries-install-nodejs

For using your own LUIS service, you can check this link.

https://docs.microsoft.com/en-us/azure/cognitive-services/luis/luis-get-started-node-get-intent

Once you have completed all the relevant works, modify `main.js` and execute the `npm start` command to run your own service.